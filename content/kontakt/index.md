**Kontakt**

- {{<anmeldung>}} für Kurse ([Dokumente]({{<ref  "/dokumente/index.md">}}))
- Tel.: [+49 8336 8590938](tel:+49-8336-8590938)
- Mobil: [+49 176 31388235](tel:+49-176-31388235)
- Mail: kontakt@musikschule-uam.de

*> Wir freuen uns bevorzugt auf Anrufe*

**Unser Team**

{{<contact name="Peter Oswald, Verwaltungsleitung" img="/avatars/peter-oswald.webp">}}
{{<contact name="Magnus Blank, Musikschulleitung"img="/avatars/magnus-blank.webp">}}