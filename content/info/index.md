## Unterricht für viele Instrumente

Wir bieten Musikstunden für Anfänger und Fortgeschrittene für viele gängigen Instrumente an. Wir unterrichten vor allem  Holz- und Blechblasinstrumente, Schlagwerk, Steich- und Zupfinstrumente, Klavier sowie Gesang an. 

In unseren Elementarkursen für Kinder  lernen die Kleinen einen ersten Umgang mit Noten und erhalten Gelegenheit, gemeinsam zu musizieren.

## Die neue Musikschule

Die neue Musikschule Unterallgäu Mitte e.V. wurde am 20.05.2019 in Erkheim gegründet. Die Musikschule bietet seit September 2019 dezentralen Musikunterricht quer durch die Mittes des Landkreises an.

Mitgliedsgemeinden sind:
- Breitenbrunn
- Erkheim
- Holzgünz
- Lauben (seit 7/2020)
- Oberrieden
- Pfaffenhausen
- Salgen
- Sontheim

## Unsere Öffnungszeiten

nach telefonischer Vereinbarung