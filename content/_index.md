---
title: Musikschule UAM
---

**Willkommen bei der Musikschule Unterallgäu Mitte e.V.**

---

{{< fun >}}
Für mehr Freude an der Musik :)
{{< /fun >}}

*Unsere Musikschule hat es sich zur Aufgabe gesetzt, die musikalische Entwicklung im Unterallgäu zu fördern und aufzubauen.*


