Einführung und Ziele 
====================

Es geht um das auffrischen der Musikschul-Website der UAM.

**Die Ziele sind:**

- Relevante Infos anzeigen, vor allem im Vergleich zu anderen Musikschulen
- Interressierte zur Anmeldung/Kontaktaufnahme bringen
- Neuigkeiten einfach bewerben

**Stakeholder:**

- Schüler
- Eltern vom Schüler
- Musikschulleiter
- Administrator
- Musiklehrer

Aufgabenstellung 
----------------

Die groben Themenbereiche sind:

- Anmeldung
- Information-Musikschule
- Rechtliches

Qualitätsziele 
--------------

| Ziel | Szenario | 
| --- | --- | 
| SEO | Jemand googelt "Trompete" und "Unterallgäu"/"Erkheim" und kommt bei der Musikschul-Webseite bei der Info zur Trompete raus. |
| Wartbarkeit | Ein Lehrer wechselt und der Aufwand zum Anpassen ist minimal, ca. 15min. | 

Kontextabgrenzung 
=================

Die Website ist erstellt mit einem Websitebaukasten (ist denke ich IONOS, 1&1).

Die Musikschule wird verwaltet mit dem Musikschul-Manager (https://www.musikschul-manager.de/). Dieses Programm verwaltet:

- *Anmeldung*
- Teilnehmer/Schüler
- Warteliste
- Lehrkräfte
- Leihinstrumente
- Notenverwaltung
- Sonstige Adressen
- Mitglieder
- ...


Fachlicher Kontext 
------------------

**Musikfächer**

Die mögl. Instrumente und Kurse sollen  den potentiellen Schülern/Eltern auf der Webseite gezeigt werden. Dabei soll man sich informieren können und den zukünftigen Lehrer kennen lernen können.  Selbes gilt für Kurse (z.B. Bläserklasse).

Damit man nicht von allen Instrumenten erschlagen wird, sind die Intrumente und Kurse vorher in Fachbereiche gruppiert (z.B. Blasinstrumente, Streichinstrumente, Grundausbildung, ...)

*Gute Beispiele:*

- https://www.musikschule-dreiklang-vbi.de/
- https://musik.memmingen.de/fachbereiche.html

![](./images/ddd-musikfächer.png)


**Aktuelles**

Hier werden die Eltern/Schüler/Interessierte auf aktuelle Themen hingewiesen, um zu zeigen das an der Musikschule "was los ist". Ebenso soll auf Fristen/Einstiegsmöglichkeiten hingewiesen werden.

![](./images/ddd-aktuelles.png)


**Anmeldung**

Hier wird über die Anmeldung informiert und der Musikschul-Manager verlinkt.

![](./images/ddd-anmeldung.png)

**Termine**

Hier wird über die Anmeldung informiert und der Musikschul-Manager verlinkt.

![](./images/ddd-termine.png)

**Kontakt**

Konkaktinformation und Leiter aufzeigen  und darauf hinweisen das auch gerne tel. werden kann. (So gut wie es jetz schon ist).

Bausteinsicht 
=============

Querschnittliche Konzepte 
=========================

Entwurfsentscheidungen 
======================

Qualitätsanforderungen 
======================

Glossar 
=======